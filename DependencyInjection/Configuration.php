<?php

namespace Maesbox\FayeWebSocketBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('maesbox_faye_web_socket');

        $rootNode
            ->children()
                ->scalarNode('node_path')->cannotBeEmpty()->end()
                ->arrayNode('server')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('name')
                                ->isRequired(true)
                            ->end()
                            ->scalarNode('host')
                                ->isRequired(true)
                            ->end()
                            ->scalarNode('port')
                                ->isRequired(true)
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
        /*$rootNode
            ->children()
                ->arrayNode('class')
                    ->children()
                        ->scalarNode('user')->cannotBeEmpty()->end()
                    //    ->scalarNode('catalogue')->cannotBeEmpty()->end()
                    ->end()
                ->end()
                /*->arrayNode("security")
                    ->children()
                        ->arrayNode("secured")
                            ->prototype('array')
                                ->children()
                                    ->scalarNode('path')
                                        ->isRequired(true)
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode("excluded")
                            ->prototype('array')
                                ->children()
                                    ->scalarNode('path')
                                        ->isRequired(true)
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();*/

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}


var host = String(process.argv[2]) || "127.0.0.1";
var port = parseInt(process.argv[3]) || 8080 ;
var http = require('http'),
    faye = require('faye');

var server = http.createServer(),
    bayeux = new faye.NodeAdapter({mount: '/faye', timeout: 45});

bayeux.attach(server);
server.listen(port, host);


bayeux.on('handshake', function(clientId) {
  console.log(Date.now(),"handshake", clientId);
});
bayeux.on('subscribe', function(clientId, channel) {
  console.log(Date.now(),"subscribe", clientId, channel);
});
bayeux.on('unsuscribe', function(clientId, channel) {
  console.log(Date.now(),"unsuscribe", clientId, channel);
});
bayeux.on('publish', function(clientId, channel, data) {
  console.log(Date.now(),"publish", clientId, channel, data);
});
bayeux.on('disconnect', function(clientId) {
  console.log(Date.now(),"disconnect", clientId);
});


console.log("server running on "+ host+":"+port);
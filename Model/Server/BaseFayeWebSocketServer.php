<?php

namespace Maesbox\FayeWebSocketBundle\Model\Server;

use Maesbox\FayeWebSocketBundle\Model\Server\Interfaces\FayeWebSocketServerInterface;
use Symfony\Component\Process\Process;


abstract class BaseFayeWebSocketServer implements FayeWebSocketServerInterface
{
	/**
	 * @var string 
	 */
	protected $name;
	
	/**
	 * @var string 
	 */
	protected $host;
	
	/**
	 * @var integer 
	 */
	protected $port;
	
	/**
	 * @var string 
	 */
	protected $lock_path;
	
	/**
	 * @var string 
	 */
	protected $node_path;
	
	/**
	 * @var string 
	 */
	protected $server_script;
	
	/**
	 * @var Process
	 */
	protected $process;


	/**
	 * @param string $name
	 * @param string $host
	 * @param integer $port
	 * @param string $lock_path
	 * @param string $node_path
	 * @param string $server_script
	 */
	public function __construct($name , $host, $port, $lock_path, $node_path, $server_script)
	{
		$this->setName($name)
			->setHost($host)
			->setPort($port)
			->setLockPath($lock_path)
			->setNodePath($node_path)
			->setServerScript($server_script);
	}
	
	/**
	 * @param Process $process
	 * @return \Maesbox\FayeWebSocketBundle\Model\Server\BaseFayeWebSocketServer
	 */
	public function setProcess(Process $process)
	{
		$this->process = $process;
		return $this;
	}
	
	/**
	 * @return Process
	 */
	public function getProcess()
	{
		return $this->process;
	}
	
	/**
	 * @return string
	 */
	public function getServerScript()
	{
		return $this->server_script;
	}
	
	/**
	 * @param string $script
	 * @return \Maesbox\FayeWebSocketBundle\Model\Server\BaseFayeWebSocketServer
	 */
	public function setServerScript($script)
	{
		$this->server_script = $script;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getHost()
	{
		return $this->host;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * @return port
	 */
	public function getPort()
	{
		return $this->port;
	}
	
	/**
	 * @param string $host
	 * @return \Maesbox\FayeWebSocketBundle\Model\Server\BaseFayeWebSocketServer
	 */
	public function setHost($host)
	{
		$this->host = $host;
		return $this;
	}

	/**
	 * @param string $name
	 * @return \Maesbox\FayeWebSocketBundle\Model\Server\BaseFayeWebSocketServer
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @param integer $port
	 * @return \Maesbox\FayeWebSocketBundle\Model\Server\BaseFayeWebSocketServer
	 */
	public function setPort($port)
	{
		$this->port = $port;
		return $this;
	}
	
	/**
	 * @param string $path
	 * @return \Maesbox\FayeWebSocketBundle\Model\Server\BaseFayeWebSocketServer
	 */
	public function setNodePath($path)
	{
		$this->node_path = $path;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getNodePath()
	{
		return $this->node_path;
	}
	
	/**
	 * @param string $path
	 * @return \Maesbox\FayeWebSocketBundle\Model\Server\BaseFayeWebSocketServer
	 */
	protected function setLockPath($path)
	{
		$this->lock_path = $path;
		return $this;
	}
	
	/**
	 * @return string
	 */
	protected function getLockPath()
	{
		return $this->lock_path;
	}
	
	/**
	 * @return string
	 */
	protected function getLockFilePath()
	{
		return $this->getLockPath()."/".$this->getName().".lock";
	}
	
	/**
	 * @param string $name
	 */
	private function removeLock()
	{
		unlink($this->getLockFilePath());
	}
	
	/**
	 * @param string $name
	 * @return boolean|integer
	 */
	private function getPidFromLock()
	{
		if(file_exists($this->getLockFilePath())){
			return file_get_contents($this->getLockFilePath());
		} else {
			return false;
		}
	}
	
	/**
	 * @param string $name
	 * @return boolean
	 */
	private function checkPidRunning()
	{
		$pid = file_get_contents($this->getLockFilePath());
		
		if($pid) {
			return $this->checkProcessByPid($pid);
		}
		
		return false;
	}
	
	/**
	 * @param string $name
	 * @return boolean
	 */
	public function isRunning()
	{
		if(file_exists($this->getLockFilePath())) {
			$status = $this->checkPidRunning();
			if(!$status) {
				$this->removeLock();
			}
			return $status;
		} else {
			return false;
		}
	}
	
	/**
	 * @param string $name
	 * @param integer|string $pid
	 */
	protected function createLock( $pid)
	{
		touch($this->getLockFilePath());
		file_put_contents ( $this->getLockFilePath() , $pid); 
	}
}

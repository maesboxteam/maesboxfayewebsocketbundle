<?php

namespace Maesbox\FayeWebSocketBundle\Model\Server;

use Symfony\Component\Process\Process;
use Maesbox\FayeWebSocketBundle\Model\Server\BaseFayeWebSocketServer;

class WindowsFayeWebSocketServer extends BaseFayeWebSocketServer 
{
	public function run()
	{
		if(!$this->isRunning())
		{
			$process = new Process('"'.$this->getNodePath().'" "'. $this->getServerScript().'" '.$this->getHost().' '.$this->getPort());
			
			$process->setTimeout(0);
			
			$process->setEnhanceWindowsCompatibility(true);
			
			$process->start();
			$pid = $process->getPid();
			$this->createLock( $pid);
			
			$this->setProcess($process);			
		} 
		
		return $this->isRunning();
	}
	
	public function stop()
	{
		if($this->isRunning())
		{
			$process = new Process('taskkill /pid '.$this->getPidFromLock()." /T /F");
			
			$process->setEnhanceWindowsCompatibility(true);
			
			$process->start();
			$this->removeLock();
		}
		
		return !$this->isRunning();
	}
	
	public function checkProcessByPid($pid)
	{
		$process = new Process('tasklist /fo list /fi "PID eq '.$pid.'"');
		$process->run();
		
		if($process->isSuccessful()) {
			if(strpos($process->getOutput(), $pid) > -1 )
			{
				return true;
			}
		}
		
		return false;
	}

	public function getStatus()
	{
		
	}

}


<?php

namespace Maesbox\FayeWebSocketBundle\Model\Server\Interfaces;


interface FayeWebSocketServerInterface
{
	public function run();
	
	public function stop();
	
	public function getStatus();
	
	public function getName();
	
	public function setName($name);
	
	public function getHost();
	
	public function setHost($host);
	
	public function getPort();
	
	public function setPort($port);
	
	public function setServerScript($script);
	
	public function getServerScript();
	
	public function checkProcessByPid($pid);
	
	public function isRunning();
}


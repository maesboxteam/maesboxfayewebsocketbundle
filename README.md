Maesbox Faye Websocket Bundle
===

##configuration

```yml
maesbox_faye_websocket:
	node_path: <node_path>
	server:
		- { name: <name>, host: <host>, port: <port> }
```

##commands

```
php console maesbox::start
```
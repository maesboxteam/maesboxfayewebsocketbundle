<?php

namespace Maesbox\FayeWebSocketBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StopWebSocketServerCommand extends ContainerAwareCommand
{
    private $current_process = null;

    protected function configure()
    {
        $this->setName('maesbox:websocket:stop')
            ->setDescription('start websocket server(s)')
            ->setHelp('This command allows you to stop a faye websocket server')
            ->addArgument('servername', InputArgument::REQUIRED, 'The name of the server')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = $this->getContainer()->get('maesbox.websocket.server.manager');

        $output->writeln(json_encode($manager->getServerList()));

        //$manager->run("test1");

        if ($manager->getServerStatus($input->getArgument('servername'))) {
            $output->writeln('stopping server: '.$input->getArgument('servername'));
            $process = $manager->stop($input->getArgument('servername'));
            $output->writeln('stopping server: '.$process->getCommandline());
            while ($process->isRunning()) {
                $out = $process->getIncrementalOutput();
                $error = $process->getIncrementalErrorOutput();
                if (!is_null($out) && $out != '') {
                    $output->writeln($out);
                }
                if (!is_null($error) && $error != '') {
                    $output->writeln($error);
                }
            }
        } else {
            $output->writeln('server not running');
        }
    }
}

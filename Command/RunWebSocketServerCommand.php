<?php

namespace Maesbox\FayeWebSocketBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RunWebSocketServerCommand extends ContainerAwareCommand
{
    private $current_process = null;

    protected function configure()
    {
        $this->setName('maesbox:websocket:start')
            ->setDescription('start websocket server(s)')
            ->setHelp('This command allows you to start faye websocket server')
            ->addArgument('servername', InputArgument::REQUIRED, 'The name of the server')
            // ...
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
		$manager = $this->getContainer()->get('maesbox.websocket.server.manager');
		
		$server = $manager->getServer($input->getArgument("servername"));
		
		if($server && !$server->isRunning()) {
			$output->writeln("starting server: ".$input->getArgument("servername"));
			if($server->run()){
				$process = $server->getProcess();
				$output->writeln("server started: ".$process->getPid());
				while($process->isRunning()){
					$out = $process->getIncrementalOutput();
					$error = $process->getIncrementalErrorOutput();
					if(!is_null($out) && $out != ""){
						$output->writeln($out);
					}
					if(!is_null($error) && $error != ""){
						$output->writeln($error);
					}
				}
			}
			
		} else {
			$output->writeln("server already running");
		}
    }
}

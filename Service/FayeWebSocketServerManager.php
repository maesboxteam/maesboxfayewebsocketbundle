<?php

namespace Maesbox\FayeWebSocketBundle\Service;

use Nc\FayeClient\Adapter\AdapterInterface;
use Nc\FayeClient\Client as FayeClient;

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Process\Process;
use Psr\Log\LoggerInterface;


use Maesbox\FayeWebSocketBundle\Model\Server\LinuxFayeWebSocketServer;
use Maesbox\FayeWebSocketBundle\Model\Server\WindowsFayeWebSocketServer;


class FayeWebSocketServerManager
{
	/**
	 * @var Kernel 
	 */
	protected $kernel;
	
	/**
	 * @var string 
	 */
	protected $node_path;
	
	/**
	 * @var array 
	 */
	protected $servers;
	
	/**
	 * @var AdapterInterface 
	 */
	protected $adapter;
	
	/**
	 *
	 * @var LoggerInterface
	 */
	protected $logger;
	
	/**
	 * 
	 * @param string $node_path
	 * @param array $servers
	 */
	public function __construct(Kernel $kernel, AdapterInterface $adapter, $node_path, array $servers, LoggerInterface $logger)
	{
		$this->setKernel($kernel)
			->setAdapter($adapter)
			->setNodePath($node_path)
			->setServerList($servers)
			->setLogger($logger);
	}
	
	/**
	 * @param string $servername
	 * @param string $channel
	 * @param array $data
	 * @param array $ext
	 */
	public function publish($servername, $channel, array $data = null, array $ext = null)
	{
		$client = $this->getClient($servername);
		
		if($client){
			$client->send($channel, $data, $ext);
		} else {
			return false;
		}
	}
	
	/**
	 * @param array $server_config
	 * @return FayeWebSocketServer
	 */
	private function createServer(array $server_config)
	{
		$host = $server_config['host'];
		$port = $server_config['port'];
		$name = $server_config['name'];
		$server_script = (isset($server_config['script']))? $server_config['script']: $this->getJsServer();
		
		if(PHP_OS=='WINNT' || PHP_OS=='WIN32' || PHP_OS=='Windows'){
			$server = new WindowsFayeWebSocketServer($name, $host, $port, $this->getLockDirectory(), $this->getNodePath(), $server_script);
		} else {
			$server = new LinuxFayeWebSocketServer($name, $host, $port, $this->getLockDirectory(), $this->getNodePath(), $server_script);
		}
		
		return $server;
	}
	
	/**
	 * @param array $servers
	 * @return \Maesbox\FayeWebSocketBundle\Service\FayeWebSocketServerManager
	 */
	public function setServerList(array $servers)
	{
		foreach ($servers as $server)
		{
			$this->servers[] = $this->createServer($server);
		}
		
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getServerList()
	{
		return $this->servers;
	}
	
	/**
	 * @return LoggerInterface
	 */
	public function getLogger()
	{
		return $this->logger;
	}
	
	/**
	 * @param LoggerInterface $logger
	 * @return \Maesbox\FayeWebSocketBundle\Lib\FayeWebSocketServerManager
	 */
	public function setLogger(LoggerInterface $logger)
	{
		$this->logger = $logger;
		return $this;
	}
	
	/**
	 * @param Kernel $kernel
	 * @return \Maesbox\FayeWebSocketBundle\Lib\FayeWebSocketServerManager
	 */
	private function setKernel(Kernel $kernel)
	{
		$this->kernel = $kernel;
		return $this;
	}
	
	/**
	 * @return Kernel
	 */
	private function getKernel()
	{
		return $this->kernel;
	}
	
	/**
	 * @param AdapterInterface $adapter
	 * @return \Maesbox\FayeWebSocketBundle\Lib\FayeWebSocketServerManager
	 */
	private function setAdapter(AdapterInterface $adapter)
	{
		$this->adapter = $adapter;
		return $this;
	}
	
	/**
	 * @return AdapterInterface
	 */
	private function getAdapter()
	{
		return $this->adapter;
	}
	
	/**
	 * @return string
	 */
	private function getJsServer()
	{
		return $this->getBinDirectory().'FayeServer.js';
	}
	
	/**
	 * @return string
	 */
	private function getBinDirectory()
	{
		return $this->getKernel()->locateResource('@MaesboxFayeWebSocketBundle/bin/');
	}
	
	/**
	 * @return string
	 */
	private function getLockDirectory()
	{
		return $this->getKernel()->locateResource('@MaesboxFayeWebSocketBundle/locks/');
	}
	
	/**
	 * @param string $node_path
	 * @return \Maesbox\FayeWebSocketBundle\Service\FayeWebSocketServerManager
	 */
	public function setNodePath($node_path)
	{
		$this->node_path = $node_path;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getNodePath()
	{
		return $this->node_path;
	}
	
	/**
	 * @param type $servername
	 * @return FayeClient|boolean
	 */
	private function getClient($servername)
	{
		$server = $this->getServer($servername);
		
		if($server){
			return new FayeClient($this->getAdapter(), 'http://'.$server->getHost().':'.$server->getPort().'/faye');
		} else {
			return false;
		}
	}
	
	/**
	 * @param string $name
	 * @return boolean|FayeWebSocketServer
	 */
	public function getServer($name)
	{
		foreach($this->getServerList() as $server)
		{
			if($server->getName() === $name){
				return $server;
			}
		}
		
		return false;
	}
	
	
	
	/**
	 * @param string $name
	 */
	public function run($name)
	{
		$server = $this->getServer($name);
		
		if($server){
			if(!$server->isRunning()) {
				return $server->run();
			}
		}
		
		return false;
		
		/*if(!$this->getServerStatus($name))
		{
			$this->getLogger()->info("starting server:".$name);
			if(PHP_OS=='WINNT' || PHP_OS=='WIN32' || PHP_OS=='Windows'){
				$command = '"'.$this->getNodePath().'" "'. $this->getJsServer().'" '.$this->getServerHost($name).' '.$this->getServerPort($name);
			} else {
				$command = '"'.$this->getNodePath().'" "'. $this->getJsServer().'" '.$this->getServerHost($name).' '.$this->getServerPort($name);
			}
			
			$process = new Process($command);
			
			$process->setTimeout(0);
			
			if(PHP_OS=='WINNT' || PHP_OS=='WIN32' || PHP_OS=='Windows'){
				$process->setEnhanceWindowsCompatibility(true);
			}
			
			$process->start();
			$pid = $process->getPid();
			$this->createLock($name, $pid);
			
			return $process;
			
		} else {
			$this->getLogger()->warning("server already running:".$name);
		}
		
		return false;*/
	}
	
	
	/**
	 * @param string $name
	 * @return boolean
	 */
	public function getServerStatus($name)
	{
		if(file_exists($this->getLockFilePath($name))) {
			$status = $this->checkPidRunning($name);
			if(!$status) {
				$this->removeLock($name);
			}
			return $status;
		} else {
			return false;
		}
	}
	
	
	
	

	
	
	
	
	
	
	/**
	 * @param string $name
	 * @return string
	 */
	private function getLockFilePath($name)
	{
		return $this->getLockDirectory().$name.".lock";
	}
	
	
	
	
	
	/**
	 * @param string $name
	 * @return string
	 */
	public function getServerHost($name)
	{
		$list = $this->getServerList();
		
		return $list[$name]['host'];
	}
	
	/**
	 * @param string $name
	 * @return string
	 */
	public function getServerPort($name)
	{
		$list = $this->getServerList();
		
		return $list[$name]['port'];
	}
	
	
}

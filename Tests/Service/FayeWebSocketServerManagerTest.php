<?php

namespace Maesbox\FayeWebSocketBundle\Tests\Service;

use Maesbox\FayeWebSocketBundle\Service\FayeWebSocketServerManager;

class FayeWebSocketServerManagerTest extends \PHPUnit_Framework_TestCase
{
	protected $manager;
	
	public function setUp()
    {
        parent::setUp();
		
		$kernel = $this->createMock("Symfony\Component\HttpKernel\Kernel");
		
		$adapter = $this->createMock("Nc\FayeClient\Adapter\CurlAdapter");
		
		$servers = [
			[
				"name" => "test1",
				"host" => "127.0.0.1",
				"port" => "8080"
			],
			[
				"name" => "test2",
				"host" => "127.0.0.1",
				"port" => "8090"
			]
		];
		
		$node_path = "plop";
		
		$logger = $this->createMock("");
		
		$this->manager = new FayeWebSocketServerManager($kernel, $adapter, $node_path, $servers, $logger);
	}
	
	public function testGetServer()
	{
		if(PHP_OS=='WINNT' || PHP_OS=='WIN32' || PHP_OS=='Windows'){
			$this->assertInstanceOf("Maesbox\FayeWebSocketBundle\Model\Server\WindowsFayeWebSocketServer", $this->manager->getServer("test1"));
		} else {
			$this->assertInstanceOf("Maesbox\FayeWebSocketBundle\Model\Server\LinuxFayeWebSocketServer", $this->manager->getServer("test1"));
		}
		
	}
	
	public function testPublishToServer()
	{
		
	}
}


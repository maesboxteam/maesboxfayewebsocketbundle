<?php


namespace Maesbox\FayeWebSocketBundle\Tests\Model;

use Maesbox\FayeWebSocketBundle\Tests\Model\FayeWebSocketServerTest;

use Maesbox\FayeWebSocketBundle\Model\Server\LinuxFayeWebSocketServer;

class LinuxFayeWebSocketServerTest extends FayeWebSocketServerTest
{
	public function setUp()
    {
        parent::setUp();
		
		$this->server = new LinuxFayeWebSocketServer("test", "127.0.0.1", 8080, "./base", "/node", "/server.js");
	}
	
	public function testNotRunningStatus()
	{
		$this->assertFalse($this->server->isRunning());
	}
	
	public function testrunningStatus()
	{
		if(PHP_OS=='WINNT' || PHP_OS=='WIN32' || PHP_OS=='Windows'){
			$this->assertFalse($this->server->isRunning());
		} else {
			$process = $this->server->run();
		
			$this->assertTrue($this->server->isRunning());
		}
	}
}
<?php


namespace Maesbox\FayeWebSocketBundle\Tests\Model;

use Maesbox\FayeWebSocketBundle\Tests\Model\FayeWebSocketServerTest;

use Maesbox\FayeWebSocketBundle\Model\Server\WindowsFayeWebSocketServer;

class WindowsFayeWebSocketServerTest extends  FayeWebSocketServerTest
{
	public function setUp()
    {
        parent::setUp();
		
		$this->server = new WindowsFayeWebSocketServer("test", "127.0.0.1", 8080, "/base", "/node", "/server.js");
	}
	
	public function testNotRunningStatus()
	{
		$this->assertFalse($this->server->isRunning());
	}
	
	public function testrunningStatus()
	{
		if(PHP_OS=='WINNT' || PHP_OS=='WIN32' || PHP_OS=='Windows'){
			$process = $this->server->run();
		
			$this->assertTrue($this->server->isRunning());
		} else {
			$this->assertFalse($this->server->isRunning());
		}
		
	}
}